var ctx = document.getElementById('{id}').getContext('2d');
var {id} = new Chart(ctx, {{
    type: 'bar',
    options: {{
        title: {{
            display: true,
            text: '{title}',
            fontSize: 26
        }},
        legend: {{
            display: false
         }},
    }},
    data: {{
        labels: {labels},
        datasets: [{{
            backgroundColor: 'rgba(255, 127, 80, 0.8)',
            borderColor: 'coral',
            borderWidth: 2,
            label: '{title}',
            data: {data}
        }}]
}}
}});