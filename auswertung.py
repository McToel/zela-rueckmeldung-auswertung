import pandas as pd 
import re

with open('website.html', 'r') as file:
    WEBSITE = file.read()

with open('title.html', 'r') as file:
    TITLE = file.read()

with open('text_answer.html', 'r') as file:
    TEXT_ANSWER = file.read()

with open('barplot_canvas.html', 'r') as file:
    BARPLOT_CANVAS = file.read()

with open('barplot_script.js', 'r') as file:
    BARPLOT_SCRIPT = file.read()

def replace_title(title):
    return title.replace(' ', '_').replace('[', '').replace(']', '') \
        .replace('"', '').replace('?', '').replace('.', '').replace('/', '') \
        .replace('*', '').replace('-', '')

def plot_dusche(data: pd.DataFrame, title: str):
    data = data[title]
    answers = pd.Series(data=[0 for _i in range(10)], index=range(1, 11))

    given_answers = data.value_counts()
    answers[given_answers.index] = given_answers

    canvas = BARPLOT_CANVAS.format(id=replace_title(title))
    script = BARPLOT_SCRIPT.format(id=replace_title(title),
                                   labels=str(answers.index.to_list()),
                                   data=str(answers.to_list()),
                                   title=title)
    return canvas, script

def plot_stimme_zu(data: pd.DataFrame, question:str, title: str):
    data = data[question]
    answers = pd.Series(data=[0 for _i in range(5)], 
        index=['Stimme nicht zu', 'Stimme wenig zu', 'neutral', 'Ja, schon irgendwie', 'Stimme voll zu'])

    given_answers = data.value_counts()
    given_answers = given_answers.rename({'Stimme gar nicht zu': 'Stimme nicht zu'}, errors='ignore')
    answers[given_answers.index] = given_answers

    canvas = BARPLOT_CANVAS.format(id=replace_title(title))
    script = BARPLOT_SCRIPT.format(id=replace_title(title),
                                   labels=str(answers.index.to_list()),
                                   data=str(answers.to_list()),
                                   title=title)
    return canvas, script

def plot_text_answers(data: pd.DataFrame, title: str):
    data = data[title]
    answers = data.value_counts()
    text_answers = ''
    for answer in answers.index:
        text_answers += TEXT_ANSWER.format(count=answers[answer], text=answer)
    return text_answers

if __name__ == "__main__":
    data = pd.read_csv('data.csv', index_col=None).drop(columns='Zeitstempel')
    data = data.drop_duplicates()
    scripts = ''
    body = ''
    last_question = ''
    for question in data.columns:
        if '[' in question:
            this_question = re.search(r'.*\?', question)[0]
            if this_question != last_question:
                body += TITLE.format(title=this_question)
            canvas, script = plot_stimme_zu(data, question, re.search(r'\[(.*)\]', question)[1])
            scripts += script
            body += canvas
            last_question = this_question
        elif 'Dusche' in question or 'diesjährige Zeltlager' in question:
            canvas, script = plot_dusche(data, question)
            scripts += script
            body += canvas
        else:
            body += TITLE.format(title=question)
            body += plot_text_answers(data, question)

    with open("templates/webchart.html", "w") as text_file:
        text_file.write(WEBSITE.format(body=body, script=scripts))